import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { Fish } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataService {

constructor(private firestore: AngularFirestore, private router: Router) { 
  
}
  createFish(fish: Fish){
    return this.firestore.collection('fish').add(fish);
  }

  getFishes() {
    return this.firestore.collection('fish').snapshotChanges();
  }

  getFish(id) {
    return this.firestore.collection('fish').doc(id).valueChanges();
  }

  updateFish(id, fish: Fish) {
    this.firestore.collection('fish').doc(id).update(fish)
      .then(() => {
        this.router.navigate(['list-fish']);
      }).catch(error => console.log(error));
  }
 
  deleteFish(id) {
    this.firestore.doc('fish/'+id).delete();
  }
 
}
