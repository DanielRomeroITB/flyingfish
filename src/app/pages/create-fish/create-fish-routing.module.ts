import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateFishPage } from './create-fish.page';

const routes: Routes = [
  {
    path: '',
    component: CreateFishPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateFishPageRoutingModule {}
