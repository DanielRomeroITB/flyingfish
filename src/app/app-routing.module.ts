import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list-fish',
    pathMatch: 'full'
  },
  {
    path: 'create-fish',
    loadChildren: () => import('./pages/create-fish/create-fish.module').then( m => m.CreateFishPageModule)
  },
  {
    path: 'list-fish',
    loadChildren: () => import('./pages/list-fish/list-fish.module').then( m => m.ListFishPageModule)
  },
  {
    path: 'edit-fish/:id',
    loadChildren: () => import('./pages/edit-fish/edit-fish.module').then( m => m.EditFishPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
