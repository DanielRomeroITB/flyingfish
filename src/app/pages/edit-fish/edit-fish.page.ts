import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-edit-fish',
  templateUrl: './edit-fish.page.html',
  styleUrls: ['./edit-fish.page.scss'],
})
export class EditFishPage implements OnInit {

  editForm: FormGroup;
  id: String;

  constructor(private dataService: DataService, private activatedRoute: ActivatedRoute, private router: Router, public formBuilder: FormBuilder) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.dataService.getFish(this.id).subscribe(
      res => {
      this.editForm = this.formBuilder.group({
        name: [res['name']],
        price: [res['price']]
      })
    });
  }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      name: [''],
      price: ['']
    })
  }
 
  onSubmit() {
    this.dataService.updateFish(this.id, this.editForm.value);
  }
}
