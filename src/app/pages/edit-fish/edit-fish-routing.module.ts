import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditFishPage } from './edit-fish.page';

const routes: Routes = [
  {
    path: '',
    component: EditFishPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditFishPageRoutingModule {}
