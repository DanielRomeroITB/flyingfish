import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CreateFishPageRoutingModule } from './create-fish-routing.module';
import { CreateFishPage } from './create-fish.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateFishPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [CreateFishPage]
})
export class CreateFishPageModule {}
