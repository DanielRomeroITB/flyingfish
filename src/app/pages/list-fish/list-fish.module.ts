import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListFishPageRoutingModule } from './list-fish-routing.module';

import { ListFishPage } from './list-fish.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListFishPageRoutingModule
  ],
  declarations: [ListFishPage]
})
export class ListFishPageModule {}
