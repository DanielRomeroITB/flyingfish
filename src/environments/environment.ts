// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyC15W0koTnQUnGc3BTK-ZlB4jfbFeetI5M",
    authDomain: "flyingfish-465ad.firebaseapp.com",
    projectId: "flyingfish-465ad",
    storageBucket: "flyingfish-465ad.appspot.com",
    messagingSenderId: "1013080038044",
    appId: "1:1013080038044:web:e328b0ef4a808d0e016b59",
    measurementId: "G-N5C771LTDX"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
