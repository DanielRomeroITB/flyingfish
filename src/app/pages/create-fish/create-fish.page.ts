import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-create-fish',
  templateUrl: './create-fish.page.html',
  styleUrls: ['./create-fish.page.scss'],
})
export class CreateFishPage implements OnInit {

  fishForm: FormGroup;

  constructor(private dataService: DataService, public formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.fishForm = this.formBuilder.group({
      name: [''],
      price: ['']
    })
 
  }

  onSubmit() {
    if (!this.fishForm.valid) {
      return false;
    } else {
      this.dataService.createFish(this.fishForm.value)
      .then(() => {
        this.fishForm.reset();
        this.router.navigate(['list-fish']);
      }).catch((err) => {
        console.log(err)
      });
    }
  }
}
