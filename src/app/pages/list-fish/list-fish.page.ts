import { Component, OnInit } from '@angular/core';
import { Fish } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-list-fish',
  templateUrl: './list-fish.page.html',
  styleUrls: ['./list-fish.page.scss'],
})
export class ListFishPage implements OnInit {

  fishes = [];

  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.dataService.getFishes().subscribe(
      res => {
        this.fishes = res.map((item) => {
          return {
            id: item.payload.doc.id,
            ... item.payload.doc.data() as Fish
          };
        })
      }
    );
  }

  deleteFish(id){
    if (window.confirm('Do you want to delete this fish?')) {
      this.dataService.deleteFish(id)
    }
  }
 
}
