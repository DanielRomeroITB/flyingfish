import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListFishPage } from './list-fish.page';

const routes: Routes = [
  {
    path: '',
    component: ListFishPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListFishPageRoutingModule {}
