import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditFishPageRoutingModule } from './edit-fish-routing.module';

import { EditFishPage } from './edit-fish.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditFishPageRoutingModule,
    ReactiveFormsModule  
  ],
  declarations: [EditFishPage]
})
export class EditFishPageModule {}
